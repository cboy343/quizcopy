var extensionActive = true;
const itemsToHighlight = document.querySelectorAll(".coderunnerexamples, table.coderunner-test-results, pre:not(.tablecell)");
document.addEventListener("click", function(e) {
    if (extensionActive == true) {
        var elem = e.target;
        var table = elem.closest(".coderunnerexamples") || elem.closest(".coderunner-test-results");
        // Check if text clicked on is part of a quiz server question table.
        if (table != null) {
            // The column that the test text is in tables is different on the test results than the example test table
            var col = -1;
            if (table.classList.contains("coderunnerexamples")) {
                col = 0;
            } else if (table.classList.contains("coderunner-test-results")) {
                col = 1;
            }

            if (col != -1) {
                getTextFromTable(table, col);
            }
        } else if (elem.tagName == "PRE") {
            getTextFromPre(elem);
        }
    }
});
document.addEventListener("keydown", function(e){
    if (e.key == "Control" || e.keyCode == 17) {
        extensionActive = false;

        for (let i = 0; i < itemsToHighlight.length; i++) {
            itemsToHighlight[i].classList.add("disabledQuizCopyHighlight");
        }
    }
});
document.addEventListener("keyup", function(e){
    if (e.key == "Control" || e.keyCode == 17) {
        extensionActive = true;
        for (let i = 0; i < itemsToHighlight.length; i++) {
            itemsToHighlight[i].classList.remove("disabledQuizCopyHighlight");
        }
    }
});
document.addEventListener("animationend", function(e) {
    if (e.animationName == "fadeOut") {
        //Once the indicator has fully faded out, remove the element.
        removeIndicator();
    }
});

function removeIndicator(alreadyFound = null) {
    if (alreadyFound == null) {
        //Find an indicator on the page and remove it
        document.querySelectorAll(".quizCopyIndicator")[0].remove();
    } else {
        //remove indicator specified by argument.
        document.body.removeChild(alreadyFound[0]);
    }
}
function copyStringToClipboard (str) { //Shamelessly stolen from https://techoverflow.net/2018/03/30/copying-strings-to-the-clipboard-using-pure-javascript/
    // Create new element
    var el = document.createElement('textarea');
    // Set value (string to be copied)
    el.value = str;
    // Set non-editable to avoid focus and move outside of view
    el.setAttribute('readonly', '');
    el.style = {position: 'absolute', left: '-9999px'};
    document.body.appendChild(el);
    // Select text inside element
    el.select();
    // Copy text to clipboard
    document.execCommand('copy');
    // Remove temporary element
    document.body.removeChild(el);
}

function drawIndicator() {
    // If indicator is already on the page, remove it
    var alreadyIndicator = document.querySelectorAll(".quizCopyIndicator");
    if (alreadyIndicator.length > 0) {
        removeIndicator(alreadyIndicator);
    }
    document.body.insertAdjacentHTML('afterbegin',
        '<div class="quizCopyIndicator">' +
            '<span>✅    Copied</span>' +
        '</div>'
    );
}
     
function getTextFromTable(table, col) {
    var testColumn = table.querySelectorAll("tbody .c"+col);
    var failCases = ["partial", "bad"];
    var isFailTests = false;
    for (let failCase of failCases) {
        var isFailTests = table.classList.contains(failCase) || table.parentNode.classList.contains(failCase);
        if (isFailTests) break;
    }
    
    if (isFailTests) var testResults = table.querySelectorAll("tbody .c0");
    var output = "";
    //Get each test code from each cell in column
    for (let i = 0; i < testColumn.length; i++) {
        if (!isFailTests || (isFailTests && testResults[i].firstChild.classList.contains("text-danger"))) {
            output += testColumn[i].textContent + "\n";
        }
    }
    copyStringToClipboard(output);
    drawIndicator();
}


function getTextFromPre(elem) {
    //Get text from pre tag
    var text = elem.textContent;
    copyStringToClipboard(text);
    drawIndicator();
}