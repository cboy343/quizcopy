# QuizCopy

## Quickly copy text from the example test code on CodeRunner to the clipboard.

<img style="display:block;margin:auto;width: 100%; max-width: 1500px" src="img/quizcopy.gif">